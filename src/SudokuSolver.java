import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class SudokuSolver {
    private int[][] numbers;
    
    public SudokuSolver() {
    	numbers = new int[][]
    	{
    		{0,9,8,5,0,6,7,2,3},
	    	{6,5,7,3,2,9,8,4,0},
	    	{1,3,2,7,8,4,6,9,5},
	    	{0,1,4,0,0,7,2,3,0},
	    	{2,8,5,6,9,3,1,7,4},
	    	{7,0,3,1,4,2,5,8,9},
	    	{8,2,9,4,6,1,3,5,7},
	    	{5,7,6,9,0,8,0,1,2},
	    	{3,0,1,2,7,5,9,6,0}
    	};
    }
    
    public SudokuSolver(String filePath){
    	try {
	    	@SuppressWarnings("resource")
			Scanner sc = new Scanner(new File(filePath));
			while (sc.hasNextLine()) {
				numbers = new int[9][9];
				for(int r = 0; r < numbers.length; r++)
		        {
		        	for(int c = 0; c < numbers[r].length; c++)
		        	{
		        		int N = sc.nextInt();
						numbers[r][c] = N;
		        	}
				}
			}
    	} catch (IOException e) { 
    		System.err.println("Caught IOException: " + e.getMessage()); 
    	}
    }
    
    /**
     * Solve the sudoku
     */
    public void sudokuSolver()
    {
    	for(int r = 0; r < numbers.length; r++)
        {
        	for(int c = 0; c < numbers[r].length; c++)
        	{
        		solve(r,c);
        	}
        }
    }
    
    /**
     * Solve
     * @param row
     * @param col
     * @return
     */
    public boolean solve(int row, int col) {
        if (row == numbers.length){
            row = 0;
            if (++col == numbers[row].length){
                return true;
            }
        }
        if (numbers[row][col] != 0) {
            return solve(row+1,col);
        }
        for (int k = 1; k <= numbers.length; k++) {
            if (isPossibleDigit(k,row,col)) {
                numbers[row][col] = k;
                if (solve(row+1,col) == true) {
                    return true;
                }
            }
        }
        numbers[row][col] = 0;
        return false;
    }
    
    /**
     * @param val
     * @param row val row position
     * @param col val column position
     * @return true if val in row and col is possible
     */
    public boolean isPossibleDigit(int val, int row, int col) {
        boolean isPossible = !isInRow(val, row) && !isInColumn(val, col) && !isInSquare(val, row, col);
        return isPossible;
    }
    
    /**
     * @param val
     * @param row
     * @return true if val is in the row
     */
    public boolean isInRow(int val, int row) {
    	boolean inRow = false;
    	for(int r = 0; r < numbers.length; r++)
        {
        	for(int c = 0; c < numbers[r].length; c++)
        	{
        		if(numbers[row][c] == val)
            	{
            		inRow = true;
            	}
        	}
        }
    	return inRow;
    }
    
    /**
     * @param val
     * @param col
     * @return true if val is in the col
     */
    public boolean isInColumn(int val, int col) {
        boolean inColumn = false;
        for(int r = 0; r < numbers.length; r++)
        {
        	if(numbers[r][col] == val)
        	{
        		inColumn = true;
        	}
        }
        return inColumn;
    }
    
    /**
     * @param val
     * @param row
     * @param col
     * @return true if val is in square of the row and col
     */
    public boolean isInSquare(int val, int row, int col) {
        boolean inSquare = false;
        for(int r = 0; r < getSquare(row, col).length; r++)
        {
        	for(int c = 0; c < getSquare(row, col)[r].length; c++)
        	{
        		if(getSquare(row, col)[r][c] == val)
        		{
        			inSquare = true;
        		}
        	}
        }
        return inSquare;
    }
    
    /**
     * @param row
     * @param col
     * @return the square of the block which position is determined by row and col
     */
    public int[][] getSquare(int row, int col) {
    	int[][] square = new int[3][3];
    	row = setToCenter(row);
    	col = setToCenter(col);
		square[0][0] = numbers[row-1][col-1]; // topleft
		square[0][1] = numbers[row-1][col  ]; // topmiddle
		square[0][2] = numbers[row-1][col+1]; // topright
		square[1][0] = numbers[row  ][col-1]; // middleleft
		square[1][1] = numbers[row  ][col  ]; // middle
		square[1][2] = numbers[row  ][col+1]; // middleright
		square[2][0] = numbers[row+1][col-1]; // botleft
		square[2][1] = numbers[row+1][col  ]; // botmiddle
		square[2][2] = numbers[row+1][col+1]; // botright
        return square;
    }
    
    /**
     * @param n
     * @return centered number relative to closest square
     */
    public int setToCenter(int n)
    {
    	if(n != 1 && n != 4 && n != 7)
		{
			if(n < 3)
			{
				n = 1;
			}
			else if(n > 2 && n < 6)
			{
				n = 4;
			}
			else
			{
				n = 7;
			}
		}
    	return n;
    }
    
    /**
     * Outputs the puzzle in the console
     */
    public void showPuzzle() {
    	for(int r = 0; r < numbers.length; r++)
    	{
    		for(int c = 0; c < numbers[r].length; c++)
    		{
    			System.out.print(numbers[r][c] + " ");
    			if(c >= numbers[r].length - 1)
    			{
    				System.out.println();
    			}
    		}
    	}
    }
    
    /**
     * @return numbers
     */
    public int[][] getNumbers()
    {
    	return numbers;
    }
}